<?php 
	$nilaiAwal = 0;
	$displayWarning = "none";
	$colorNilaiAwal = ["#767676", "#f1f1f1"] ;
	$colorNamaInduknya = ["#767676", "#f1f1f1"] ;
	$colorNamaHewan = ["#767676", "#f1f1f1"] ;
	$value = ["", ""];
	$warning = "";
	$warningValidasi = 1;


	if(isset($_POST["buttonSubmit"]) && empty($_POST["inputNilaiAwal"])){
		$displayWarning = "flex";
		$colorNilaiAwal[1] = "red";
		$warning = "<b>Warning:</b><br>$warningValidasi. Masukan Jumlah Hewannya<br>";
		$warningValidasi++;
	}//else if(isset($_POST["buttonSubmit"]) && ($_POST["inputNilaiAwal"]) == false){
		//$warning = "<b>Warning:</b><br>Banyak Hewannya Harus Bertipe Bilangan Bulat (int)";
	//}

	if(isset($_POST["buttonSubmit"]) && empty($_POST["inputNamaInduknya"])){
		$colorNamaInduknya[1] = "red";
		if($warningValidasi == 1){
			$warning = "<b>Warning:</b><br>$warningValidasi. Masukan Nama Induknya<br>";
		}else if($warningValidasi > 1){
			$warning .= "$warningValidasi. Masukan Nama Induknya<br>";
		}
		$warningValidasi++;
	}else if(isset($_POST["buttonSubmit"]) && strip_tags($_POST["inputNamaInduknya"]) == ""){
		$warnig .= "<b>Warning:</b><br>$warningValidasi. G Boleh Iseng Input Begituan !";
	}

	if(isset($_POST["buttonSubmit"]) && empty($_POST["inputNamaHewan"])){
		$colorNamaHewan[1] = "red";
		if($warningValidasi == 1){
			$warning = "<b>Warning:</b><br>$warningValidasi. Pilih Hewannya Terlebih Dahulu<br>";
		}else if($warningValidasi > 1){
			$warning .= "$warningValidasi. Pilih Hewannya Terlebih Dahulu<br>";
		}
		$warningValidasi++;
	}

	if(isset($_POST["buttonSubmit"]) && !empty($_POST["inputNilaiAwal"])){
		$value[0] = $_POST["inputNilaiAwal"];
	}

	if(isset($_POST["buttonSubmit"]) && !empty($_POST["inputNamaInduknya"])){
		$value[1] = $_POST["inputNamaInduknya"];
	}

	if(isset($_POST["buttonReset"])){
		for($i = 0; $i < count($value); $i++){
			$value[$i] = "";
		}
	}

?>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="style.css">
	<title>
		Anak Ayam
	</title>
	<style type="text/css">
		.warning{
			display: <?= $displayWarning; ?>;
		}
		.inputNilaiAwal{
			color: <?= $colorNilaiAwal[0]; ?>;
			border: .3vh solid <?= $colorNilaiAwal[1]; ?>;
		}

		.inputNamaInduknya{
			color: <?= $colorNamaInduknya[0]; ?>;
			border: .3vh solid <?= $colorNamaInduknya[1]; ?>;
		}

		.inputNamaHewan{
			color: <?= $colorNamaHewan[0]; ?>
			background: <?= $colorNamaHewan[1]; ?>;
		}
	</style>
</head>
<body>
	<div class="box">
		<div class="boxInput">
			<form method="POST" action="">
				<table>
					<tr>
						<td colspan="2">
							<input type="text" name="inputNilaiAwal" placeholder="Ketik Jumlah Turunnya" value="<?= $value[0]; ?>" class="inputNilaiAwal">
						</td>
					</tr>
					<tr>
						<td colspan="2">
							<input type="text" name="inputNamaInduknya" placeholder="Ketik Nama Induknya" value="<?= $value[1]; ?>" class="inputNamaInduknya">
						</td>
					</tr>
					<tr>
						<td class="inputNamaHewan" colspan="2">
							<input type="radio" id="radioAyam" name="inputNamaHewan" value="Ayam">
							<label for="radioAyam">Ayam</label>
							<input type="radio" id="radioAngsa" name="inputNamaHewan" value="Angsa">
							<label for="radioAngsa">Angsa</label>
							<input type="radio" id="radioBebek" name="inputNamaHewan" value="Bebek">
							<label for="radioBebek">Bebek</label>
						</td>
					</tr>
					<tr>
						<td>
							<input type="submit" name="buttonSubmit" value="Tampilkan" class="buttonSubmit">
						</td>
						<td>
							<input type="submit" name="butonReset" value="Reset" class="buttonReset">
						</td>
					</tr>
				</table>
			</form>
		</div>	
		<div class="boxWarning">
			<?= $warning; ?>
		</div>
		<div class="boxPowered">
			Powered By Abdul Fattah H
		</div>
		</div>
	<?php
		if(isset($_POST["buttonSubmit"]) && !empty($_POST["inputNilaiAwal"]) == true && !empty($_POST["inputNamaInduknya"]) && !empty($_POST["inputNamaHewan"])){
			$nilaiAwal = $_POST["inputNilaiAwal"];
			$nilaiawal = intval($nilaiAwal);
			$namaInduknya = strip_tags($_POST["inputNamaInduknya"]);
			$namaHewan = $_POST["inputNamaHewan"];
			prosesAnakayam($nilaiAwal, $namaInduknya, $namaHewan);
		}
	?>
</body>
</html>
<?php

	function prosesAnakAyam(string $a = "0", string $b = "", string $c = ""){
		$headerTextInduk = trim(strip_tags(strtoupper($b)));
		$indukText = trim(strip_tags(ucwords($b)));
		$headerTextHewan = trim(strip_tags(strtoupper($c)));
		$hewanText = trim(strip_tags(ucwords($c)));

		if(empty($a) || $a == ""){
			$a = 0;
		}

		if(empty($b) || $b == ""){
			$headerTextInduk = "INDUK";
			$indukText = "Induk";
		}

		echo "<div class=\"boxResult\">";
			for($i = $a; $i >= 1; $i--){
				$sisa = $i-1;
				if($i == $a){
					echo "<div class=\"headerResult\">
							  <span class=\"textResult\">RESULT</span>
							  <span class=\"textJumlahHewan\">
							  	  BANYAK ANAK $headerTextHewan YANG TURUN $i<br>
							  	  NAMA INDUKNYA ADALAH $headerTextInduk<br>
							  	  HEWAN YG TURUN ADALAH $headerTextHewan
							  </span>
						  </div>";
					echo "<div class=\"result\">";
					if($i == 1){
						echo "Anak $hewanText Turun $i, Mati 1 Tinggalah $indukText nya !";
						continue;
					}
					echo "Anak $hewanText Turun $i, Mati 1 Tinggalah $sisa<br>";
				}else{
					if($i == 1){
						echo "Anak $hewanText Turun $i, Mati 1 Tinggalah $indukText"."nya !";
						continue;
					}
					echo "Anak $hewanText Turun $i, Mati 1 Tinggalah $sisa</br>";
				}
			}
			echo "</div>";
		echo "</div>";
	}

?>

